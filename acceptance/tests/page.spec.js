describe('Concursong', () => {
  it('shows playlist title and duration', () => {
    cy.visit('/')

    cy.get('concursong-playlist').should(playlist => {
      const [dom] = playlist.get()
      const shadowRoot = dom.shadowRoot

      expect(shadowRoot.querySelector('.title').innerText).equal('Metal')
      expect(shadowRoot.querySelector('.duration').innerText).equal('1 h. 14 min')
    })
  })
})
