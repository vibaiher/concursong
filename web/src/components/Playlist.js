const template = document.createElement('template')

template.innerHTML = `
  <section class="playlist">
    <span class="title"></span>
    <span class="duration"></span>
  </section>
`

class Playlist extends HTMLElement {
  constructor() {
    super()

    this._shadowRoot = this.attachShadow({ mode: 'open' })
    this._shadowRoot.appendChild(template.content.cloneNode(true))

    this.$title = this._shadowRoot.querySelector('span.title');
    this.$duration = this._shadowRoot.querySelector('span.duration');
  }

  get title() {
    return this.getAttribute('title');
  }

  get duration() {
    return this.getAttribute('duration');
  }

  static get observedAttributes() {
    return ['title', 'duration']
  }

  attributeChangedCallback(_name, _oldVal, _newVal) {
    this.render();
  }

  render() {
    this.$title.innerHTML = this.title;
    this.$duration.innerHTML = this.duration;
  }
}

export default Playlist
