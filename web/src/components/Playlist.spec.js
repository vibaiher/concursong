import Playlist from './Playlist'

describe('Playlist', () => {
    beforeEach(() => {
        window.customElements.define('concursong-playlist', Playlist)
    })

    it('attributes are empty by default', () => {
        const playlist = document.createElement('concursong-playlist')
        
        playlist.setAttribute('title', 'My Playlist')

        expect(playlist.shadowRoot.querySelector('.title').innerHTML).toBe('My Playlist')
    })
})