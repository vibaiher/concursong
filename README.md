# ConcurSong

¿Quieres saber quién sabe más de música?

## Desarrollo

### Requisitos

- docker
- docker-compose

### ¿Cómo levantar la aplicación?

Para levantar la aplicación ejecuta este comando:

```bash
docker-compose up --build web
```

Una vez acabe de construir la aplicación, podrás abrir en tu navegador la url [localhost:1234](http://localhost:1234) y deberías ver la web funcionando.

### ¿Y los tests?

Estoy probando a testear los web componentes de manera unitaria. Jest todavía no tiene soporte oficial, pero usando la última version de jsdom y un environment especifico si que puedes interactuar con `window.customElements`. Los tests se ejecutan así:

```bash
docker-compose run --rm web npm run test
```

Tambien hay unos tests muy sencillos de aceptación, con cypress. Puedes ejecutarlos así:

```bash
docker-compose run --rm acceptance
```
